const imageCount = 423;

var Wallpaper = React.createClass({
	render(){
		var arrImages = new Array(imageCount);
		for (let i=0; i<arrImages.length; i++){
			arrImages[i] = i;
		}
		arrImages = arrImages.map((item, index)=>{
			return(
				<img key={index} src={`src/img/${index+1}.jpg`} />
			)
		})
		return(
			<div className="root-img">
				{arrImages}
			</div>
		);	
	}
});

ReactDOM.render(
	<Wallpaper />, 
	document.getElementById('root')
);