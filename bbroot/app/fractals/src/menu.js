var menu = document.getElementById('menu');
var menuItems = menu.getElementsByClassName('item');
menuItems[0].style.backgroundColor = 'rgb(148, 153, 246)';
menuItems[1].style.backgroundColor = 'rgb(148, 246, 175)';
menuItems[2].style.backgroundColor = 'rgb(246, 236, 148)';
menuItems[3].style.backgroundColor = 'rgb(246, 148, 148)';
var changeVisible = function(elem, type) {
  elem.style.visibility = type;
};

menuItems[0].onclick = function() {
  if (type != 'MANDELBROT') {
    scale = 1;
    changeVisible(load, 'visible');
    type = 'MANDELBROT';

    setTimeout(function() {
      render(scale, type);
    }, 100);

    setTimeout(function() {
      changeVisible(load, 'hidden');
    }, 160);
  }
};

menuItems[1].onclick = function() {
  if (type != 'JULIA') {
    scale = 1;
    changeVisible(load, 'visible');
    type = 'JULIA';

    setTimeout(function() {
      render(scale, type);
    }, 100);

    setTimeout(function() {
      changeVisible(load, 'hidden');
    }, 160);
  }
};

menuItems[2].onclick = function() {
  if (type != 'SHIP') {
    scale = 1;
    changeVisible(load, 'visible');
    type = 'SHIP';

    setTimeout(function() {
      render(scale, type);
    }, 100);

    setTimeout(function() {
      changeVisible(load, 'hidden');
    }, 160);
  }
};

menuItems[3].onclick = function() {
  if (type != 'CARPET') {
    scale = 1;
    changeVisible(load, 'visible');
    type = 'CARPET';

    setTimeout(function() {
      render(scale, type);
    }, 100);

    setTimeout(function() {
      changeVisible(load, 'hidden');
    }, 160);
  }
};
$(function() {
    $("#myCanvas").draggable();
});
