var b = document.getElementsByTagName("body")[0];
canvas = document.getElementById("myCanvas");
var load = document.getElementById('load');
var width = b.clientWidth;
var height =b.clientHeight;
var scale = 1;
var type = '';
var TYPES = {
  MANDELBROT: 'MANDELBROT',
  JULIA: 'JULIA',
  SHIP: 'SHIP',
  CARPET: 'CARPET'
}
canvas.width = 0;
canvas.height = 0;
context = canvas.getContext('2d');
function checkCanvasIsSupported() {

	if (canvas.getContext) {

		render(scale, type);
	} else {
		alert("Sorry, but your browser doesn't support a canvas.");
	}
}
var resize = function(e) {
  scale -= e.deltaY / 1000;
  if (scale <= 0.1) {
    scale = 0.1;
  }
  if (scale > 0.1) {
    canvas.style.transform = 'scale('+scale+')';
  }
};
canvas.addEventListener("wheel", resize);
document.getElementById('draw').addEventListener("wheel", resize);


function render(scale, type) {
  document.getElementById('welcome').style.visibility = 'hidden';
  document.body.style.backgroundColor = 'black';
  canvas.width = width;
  canvas.height = height;
  canvas.style.transform = 'scale(1)';
	context.clearRect(0, 0, canvas.width , canvas.height);
  switch (type) {
    case TYPES.MANDELBROT:
      drawMandelbrot(scale);
      break;
    case TYPES.JULIA:
      drawJulia(scale);
      break;
    case TYPES.SHIP:
      drawBurningShipFractal(scale);
      break;
    case TYPES.CARPET:
      drawSierpinskiCarpet(scale);
      break;
    default:
      drawMandelbrot(scale);
      break;
  }
};

function drawMandelbrot(scale) {
	// prepare image and pixels
	var image_data = context.createImageData(canvas.width, canvas.height);
	var d = image_data.data;

	max_iterations = 100;
	for (var i = 0; i < canvas.height; i++) {
		for (var j = 0; j < canvas.width; j++) {

			// limit the axis
			x0 = -2.0 + j * 3.0 / canvas.width * scale;
			y0 = -1.0 + i * 2.0 / canvas.height * scale;
			x = 0;
			y = 0;
			iteration = 0;
			while ((x * x + y * y < 4) && (iteration < max_iterations)) {
				x_n = x * x - y * y + x0;
				y_n = 2 * x * y + y0;
				x = x_n;
				y = y_n;
				iteration++;
			}
			// set pixel color [r,g,b,a]
			d[i * canvas.width * 4 + j * 4 + 0] = iteration*15;
			d[i * canvas.width * 4 + j * 4 + 1] = iteration*3;
			d[i * canvas.width * 4 + j * 4 + 2] = iteration*5;
			d[i * canvas.width * 4 + j * 4 + 3] = 255;
		}
	}
	// draw image
	context.putImageData(image_data, 0, 0);
}

function drawJulia(scale) {
	// prepare image and pixels
	var image_data = context.createImageData(canvas.width, canvas.height);
	var d = image_data.data;

	x0 = -0.4;
	y0 = -0.6;
	max_iterations = 100;
	for (var i = 0; i < canvas.height; i++) {
		for (var j = 0; j < canvas.width; j++) {

			// limit the axis
			x = -1.5 + j * 3.0 / canvas.width * scale;
			y = -1.0 + i * 2.0 / canvas.height * scale;

			iteration = 0;

			while ((x * x + y * y < 4) && (iteration < max_iterations)) {
				x_n = x * x - y * y + x0;
				y_n = 2 * x * y + y0;
				x = x_n;
				y = y_n;
				iteration++;
			}

			// set pixel color [r,g,b,a]
			d[i * canvas.width * 4 + j * 4 + 0] = iteration*25;
			d[i * canvas.width * 4 + j * 4 + 1] = iteration*5;
			d[i * canvas.width * 4 + j * 4 + 2] = iteration*8;
			d[i * canvas.width * 4 + j * 4 + 3] = 255;
		}
	}

	// draw image
	context.putImageData(image_data, 0, 0);
}

function drawBurningShipFractal(scale) {
	// prepare image and pixels
	var image_data = context.createImageData(canvas.width, canvas.height);
	var d = image_data.data;

	max_iterations = 100;
	for (var i = 0; i < canvas.height; i++) {
		for (var j = 0; j < canvas.width; j++) {

			x0 = -1.80 + j * (-1.7+1.80) / canvas.width * scale;
			y0 = -0.08 + i * (0.01+0.08) / canvas.height * scale;
			x = 0;
			y = 0;
			iteration = 0;

			while ((x * x + y * y < 4) && (iteration < max_iterations)) {
				x_n = x * x - y * y + x0;
				y_n = 2 * Math.abs(x * y) + y0;
				x = x_n;
				y = y_n;
				iteration++;
			}

			// set pixel color [r,g,b,a]
			d[i * canvas.width * 4 + j * 4 + 0] = 25+iteration*30;
			d[i * canvas.width * 4 + j * 4 + 1] = 25+iteration*10;
			d[i * canvas.width * 4 + j * 4 + 2] = 85-iteration*5;
			d[i * canvas.width * 4 + j * 4 + 3] = 255;
		}
	}

	// draw image
	context.putImageData(image_data, 0, 0);
}

function drawSierpinskiCarpet(scale) {
  document.getElementById("myCanvas").style.backgroundColor = 'black';
	// draw carpet
	var draw_carpet = function (x, y, width, height, iteration) {
		if (iteration == 0) return;
		var w = width / 3;
		var h = height / 3;

		// draw subsquare
		context.fillStyle = 'rgb(255,255,255)';
		context.fillRect(x + w, y + h, w, h);

		// draw subcarpets
		for (var i = 0; i < 3; i++) {
			for (var j = 0; j < 3; j++) {
				// remove central subsquare
				if (j == 1 && i == 1) continue;
				draw_carpet(x + j * w, y + i * h, w, h, iteration - 1);
			}
		}
	}

	// init carpet size
	var carpet_width = canvas.height;
	var carpet_height = canvas.height;
	// align to the center
	var carpet_left = (canvas.width - carpet_width) / 2;
	// limit the depth of recursion
  var max_iterations = 4;
  // if (scale % 0.5 == 0 && scale >= 1) {
  //   console.log('++');
  //   max_iterations++;
  // }
  // if (scale % 0.5 == 0 && scale < 1) {
  //   console.log('--');
  //   max_iterations--;
  // }

	// draw Sierpinski carpet
	draw_carpet(carpet_left, 0, carpet_width / scale, carpet_height / scale, max_iterations);
}
