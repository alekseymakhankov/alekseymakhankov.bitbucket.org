var gulp = require('gulp');
var notify = require('gulp-notify');
var browserSync = require('browser-sync');
const autoprefixer = require('gulp-autoprefixer');
var reload = browserSync.reload;

var paths = {
  html:['./**/*.html'],
  css:['./css/**/*.css'],
  js:['./js/**/*.js']
};

gulp.task('mincss', function(){
  return gulp.src(paths.css)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./src'))
    .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
    .pipe(reload({stream:true}));
});
gulp.task('html', function(){
  gulp.src(paths.html)
  .pipe(reload({stream:true}));
});
gulp.task('css', function(){
  gulp.src(paths.css)
  .pipe(reload({stream:true}));
});
gulp.task('js', function(){
  gulp.src(paths.js)
  .pipe(reload({stream:true}));
});
gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: "./"
    },
    port: 8080,
    open: true,
    notify: false
  });
});
gulp.task('watcher',function(){
  gulp.watch(paths.css, ['css']);
  gulp.watch(paths.html, ['html']);
  gulp.watch(paths.js, ['js']);
});


gulp.task('default', ['watcher', 'browserSync']);
